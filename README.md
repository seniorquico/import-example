# import-example

This simple web application demonstrates dynamically importing modules with System.js from a separate server.

## Build and Run

First, transpile the sources (replaces ES6 modules with System.js modules):

```
npm run-script build-core
npm run-script build-plugins
```

Second, run the web servers:

```
npm run-script run-core
npm run-script run-plugins
```

Finally, load the web application by navigating to <http://localhost:3000> in Chrome. Open the developer tools and confirm "blue", "green", and "red" were printed to the console. Additionally, confirm the "blue.js", "green.js", and "red.js" files were loaded from <http://localhost:3001>.
