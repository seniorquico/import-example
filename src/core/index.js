import Logger from './Logger.js'

const logger = new Logger()

System.import('plugins/blue.js').then((module) => logger.log(module.default))
System.import('plugins/green.js').then((module) => logger.log(module.default))
System.import('plugins/red.js').then((module) => logger.log(module.default))